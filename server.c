/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/03 04:08:32 by rauer             #+#    #+#             */
/*   Updated: 2023/06/15 01:26:28 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf/ft_printf.h"
#include "libft/libft.h"
#include "server.h"
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

void	flush_message(t_process *p)
{
	write(1, &(p->message), p->l);
	p->l = 0;
}

void	new_process(t_process *p, int pid)
{
	if (p->l > 0)
	{
		flush_message(p);
		write(1, "\n", 1);
	}
	p->b = 8;
	p->c = 0;
	p->pid = pid;
	ft_printf("Message from %i:\n", pid);
}

void	handler(int sig, siginfo_t *info, void *context)
{
	static t_process	p;

	(void)context;
	if (p.pid != info->si_pid)
		new_process(&p, info->si_pid);
	p.b--;
	if (sig == SIGUSR2)
		p.c |= 1 << p.b;
	if (p.b == 0 && p.c == 0)
	{
		flush_message(&p);
		write(1, "\n", 1);
		kill(p.pid, SIGUSR2);
		p.pid = -1;
	}
	if (p.l == BUFFER_SIZE)
		flush_message(&p);
	if (p.b == 0)
	{
		p.message[p.l++] = p.c;
		p.c = 0;
		p.b = 8;
	}
}

int	main(int argc, char **argv)
{
	int					pid;
	struct sigaction	sausr;

	(void)argc;
	(void)argv;
	pid = getpid();
	ft_printf("PID: %d\n", pid);
	sigemptyset(&sausr.sa_mask);
	sausr.sa_sigaction = handler;
	sausr.sa_flags = SA_SIGINFO;
	sigaction(SIGUSR1, &sausr, NULL);
	sigaction(SIGUSR2, &sausr, NULL);
	while (1)
		pause();
	return (0);
}
