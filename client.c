/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/13 18:17:41 by rauer             #+#    #+#             */
/*   Updated: 2023/06/15 01:37:15 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf/ft_printf.h"
#include "libft/libft.h"
#include <signal.h>
#include <unistd.h>

void	handler(int sig, siginfo_t *info, void *context)
{
	(void)sig;
	(void)info;
	(void)context;
	ft_printf("Message delivered\n");
}

void	write_message(int pid, char *str)
{
	int	bit;

	bit = 8;
	while (1)
	{
		if (*str & (1 << --bit))
			kill(pid, SIGUSR2);
		else
			kill(pid, SIGUSR1);
		if (bit == 0 && !*str)
			return ;
		if (bit == 0)
			str++;
		if (bit == 0)
			bit = 8;
		usleep(500);
	}
}

int	main(int argc, char **argv)
{
	int					pid;
	struct sigaction	sa;

	if (argc != 3)
		return (1);
	pid = atoi(argv[1]);
	if (pid < 1)
		return (1);
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = handler;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGUSR1, &sa, NULL);
	sigaction(SIGUSR2, &sa, NULL);
	write_message(pid, argv[2]);
	pause();
}
