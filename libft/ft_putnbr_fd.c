/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/19 00:49:10 by rauer             #+#    #+#             */
/*   Updated: 2023/01/20 16:20:04 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static void	ft_recursive(int nbr, int fd)
{
	int		quotient;
	int		mod;
	char	c;

	quotient = nbr / 10;
	mod = nbr % 10;
	if (quotient != 0)
		ft_recursive(quotient, fd);
	if (mod < 0)
		mod = -mod;
	c = mod + '0';
	write(fd, &c, 1);
}

void	ft_putnbr_fd(int n, int fd)
{
	if (n < 0)
		write(fd, "-", 1);
	ft_recursive(n, fd);
}
