/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 22:36:56 by rauer             #+#    #+#             */
/*   Updated: 2023/06/14 20:42:50 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **lst, t_list *entry)
{
	t_list	*l;

	if (!lst)
		return ;
	if (!*lst)
		*lst = entry;
	else
	{
		l = ft_lstlast(*lst);
		l->next = entry;
	}
}
