/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstremove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 22:42:57 by rauer             #+#    #+#             */
/*   Updated: 2023/06/15 00:32:32 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstremove(t_list **lst, t_list *entry, void (*del)(void *))
{
	t_list	**tmp;

	if (!lst || !*lst || !entry || !del)
		return ;
	tmp = lst;
	while (*tmp && *tmp != entry)
		tmp = &((*tmp)->next);
	if (*tmp != entry)
		ft_lstdelone(entry, del);
}
