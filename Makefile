# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: racherom <racherom@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/22 20:19:47 by rauer             #+#    #+#              #
#    Updated: 2023/06/15 01:23:15 by racherom         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minitalk
SERVER_SRC = server.c
CLIENT_SRC = client.c
SERVER_OBJ = $(SERVER_SRC:.c=.o)
CLIENT_OBJ = $(CLIENT_SRC:.c=.o)
LIB = ft_printf libft
LIB_FILES = $(addprefix inc/lib, $(addsuffix .a, $(LIB)))
LIB_FLAGS = -Linc $(addprefix -l, $(LIB)) $(addprefix -I, $(LIB))
GCC = gcc -Wall -Wextra -Werror
DEBUG = -g3 -fsanitize=address

$(NAME): all

server: $(LIB_FILES) $(SERVER_OBJ) inc
	$(GCC) $(DEBUG) $(LIB_FLAGS) $(SERVER_OBJ) -o $@

client: $(LIB_FILES) $(CLIENT_OBJ) inc
	$(GCC) $(DEBUG) $(LIB_FLAGS) $(CLIENT_OBJ) -o $@

all: server client

.PHONY: all clean fclean

$(LIB_FILES): inc/lib%.a: inc FORCE
	@make -C $* LIB_PATH=../inc/lib DEBUG="$(DEBUG)"

FORCE: ;

inc:
	mkdir inc

%.o: %.c
	$(GCC) $(DEBUG) -c $< -o $@

clean_%:
	make -C $* clean

clean: $(addprefix clean_, $(LIB))
	rm -rf $(SERVER_OBJ) $(CLIENT_OBJ)

fclean_%:
	make -C $* fclean

fclean: clean
	rm -f server client
	rm -rf inc

re: fclean all
