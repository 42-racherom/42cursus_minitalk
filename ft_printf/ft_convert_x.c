/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_x.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 21:12:22 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:32:17 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf.h"

int	ft_convert_x(int fd, va_list *ptr, t_flags flags)
{
	unsigned int	i;

	i = va_arg(*ptr, unsigned int);
	if (i == 0)
		flags.flags &= ~4;
	return (ft_flags_puthex(fd, i, flags, 'a'));
}
