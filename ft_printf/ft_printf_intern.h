/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/22 20:04:56 by rauer             #+#    #+#             */
/*   Updated: 2023/01/23 22:02:45 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_INTERN_H
# define FT_PRINTF_INTERN_H

# include <stdio.h>
# include <unistd.h>
# include <stdarg.h>

typedef struct s_flags
{
	int	offset;
	int	precision;
	int	width;
	int	flags;
}		t_flags;

t_flags	ft_parse_flags(char const *flags, va_list *ptr);
int		ft_fill(int fd, char c, int len);
int		ft_flags_befor(int fd, t_flags flags, int len);
int		ft_flags_after(int fd, t_flags flags, int len);
int		ft_putnull(int fd);
int		ft_putchar(int fd, char c);
int		ft_putstr(int fd, char *str);
int		ft_flags_puthex(int fd, unsigned long i, t_flags flags, char h);
int		ft_flags_putdeci(int fd, unsigned long i, t_flags flags, int negativ);
int		ft_convert_c(int fd, va_list *ptr, t_flags flags);
int		ft_convert_s(int fd, va_list *ptr, t_flags flags);
int		ft_convert_i(int fd, va_list *ptr, t_flags flags);
int		ft_convert_u(int fd, va_list *ptr, t_flags flags);
int		ft_convert_x(int fd, va_list *ptr, t_flags flags);
int		ft_convert_xx(int fd, va_list *ptr, t_flags flags);
int		ft_convert_p(int fd, va_list *ptr, t_flags flags);
int		ft_conversions(int fd, va_list *ptr, int *len, const char *format);

#endif